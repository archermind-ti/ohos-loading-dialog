/*
 *    Copyright 2015 Kaopiz Software Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.kaopiz.progresshud.kprogresshud;


import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class AnnularView extends Component implements Determinate,Component.EstimateSizeListener,Component.DrawTask {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(HiLog.LOG_APP, 0, "AnnularView");
    private Paint mWhitePaint;
    private Paint mGreyPaint;
    private RectFloat mBound;
    private int mMax = 100;
    private int mProgress = 0;


    public AnnularView(Context context) {
        super(context);
        init(context);
    }

    public AnnularView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AnnularView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        setEstimateSizeListener(this);
        addDrawTask(this);
        mWhitePaint = new Paint();
        mWhitePaint.setAntiAlias(true);
        mWhitePaint.setStyle(Paint.Style.STROKE_STYLE);
        mWhitePaint.setStrokeWidth(AttrHelper.fp2px(3,getContext()));
        mWhitePaint.setColor(Color.WHITE);

        mGreyPaint = new Paint();
        mGreyPaint.setAntiAlias(true);
        mGreyPaint.setStyle(Paint.Style.STROKE_STYLE);
        mGreyPaint.setStrokeWidth(AttrHelper.fp2px(3,getContext()));
        mGreyPaint.setColor(new Color(context.getColor(ResourceTable.Color_kprogresshud_grey_color)));

        mBound = new RectFloat();

        setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                int padding = AttrHelper.fp2px(4, getContext());
                mBound.modify(padding, padding, getWidth() - padding, getHeight() - padding);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });


    }


    @Override
    public void onDraw(Component component, Canvas canvas) {

        float mAngle = mProgress * 360f / mMax;
        Arc arc  = new Arc(270f, mAngle, false);
        canvas.drawArc(mBound,arc,mWhitePaint);
        Arc arc2  = new Arc( 270f + mAngle, 360f - mAngle, false);
        canvas.drawArc(mBound,arc2,mGreyPaint);

    }


    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {

        int dimension = AttrHelper.vp2px(40, getContext());
        setEstimatedSize(
                Component.EstimateSpec.getSizeWithMode(dimension, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getSizeWithMode(dimension, Component.EstimateSpec.NOT_EXCEED));
        return true;

    }

    @Override
    public void setMax(int max) {
        this.mMax = max;
    }

    @Override
    public void setProgress(int progress) {
        mProgress = progress;
        invalidate();
    }


}

/*
 *    Copyright 2015 Kaopiz Software Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.kaopiz.progresshud.kprogresshud;


import ohos.agp.colors.RgbColor;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class BackgroundLayout extends StackLayout implements Component.DrawTask  {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(HiLog.LOG_APP, 0, "BackgroundLayout");
    private float mCornerRadius;
    private Paint mPaint;
    private RectFloat mRect;

    public BackgroundLayout(Context context) {
        super(context);
        init();
    }

    public  BackgroundLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public BackgroundLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {

        setBaseColor(getContext().getColor(ResourceTable.Color_kprogresshud_default_color));

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(RgbColor.fromArgbInt(ResourceTable.Color_transparent)));//0x00000000
        //setBackground(shapeElement);
        setBackground(new ShapeElement(){{setRgbColor(RgbPalette.TRANSPARENT);}});
        addDrawTask(this, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);

        setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                int estimatedHeight =   component.getEstimatedHeight();
                int estimatedWidth =   component.getEstimatedWidth();
                mRect = new RectFloat(0, 0, estimatedWidth, estimatedHeight);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });

    }

    public void setCornerRadius(float radius) {
        mCornerRadius = AttrHelper.vp2px(radius,getContext());
    }

    public void setBaseColor(int color) {
        mPaint = new Paint();
        mPaint.setColor(new Color(color));
        mPaint.setStyle(Paint.Style.FILL_STYLE);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawRoundRect(mRect, mCornerRadius, mCornerRadius, mPaint);
    }
}

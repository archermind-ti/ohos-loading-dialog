/*
 *    Copyright 2015 Kaopiz Software Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.kaopiz.progresshud.kprogresshud;


import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class BarView extends Component implements Determinate,Component.EstimateSizeListener,Component.DrawTask {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(HiLog.LOG_APP, 0, "BarView");
    private Paint mOuterPaint;
    private Paint mInnerPaint;
    private RectFloat mOutBound;
    private RectFloat mInBound;
    private int mMax = 100;
    private int mProgress = 0;
    private float mRadius;
    private float mBoundGap;


    public BarView(Context context) {
        super(context);
        init();
    }

    public BarView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public BarView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        setEstimateSizeListener(this);
        addDrawTask(this);
        mOuterPaint = new Paint();//Paint.ANTI_ALIAS_FLAG
        mOuterPaint.setAntiAlias(true);
        mOuterPaint.setStyle(Paint.Style.STROKE_STYLE);
        mOuterPaint.setStrokeWidth(AttrHelper.vp2px(2,getContext()));//Helper.dpToPixel(2, getContext())
        mOuterPaint.setColor(Color.WHITE);

        mInnerPaint = new Paint();//Paint.ANTI_ALIAS_FLAG
        mInnerPaint.setAntiAlias(true);
        mInnerPaint.setStyle(Paint.Style.FILL_STYLE);
        mInnerPaint.setColor(Color.WHITE);

        mBoundGap = AttrHelper.vp2px(5,getContext());//Helper.dpToPixel(5, getContext());
        mInBound = new RectFloat(mBoundGap, mBoundGap,
                (getWidth() - mBoundGap) * mProgress / mMax, getHeight() - mBoundGap);

        mRadius = AttrHelper.vp2px(10,getContext());//Helper.dpToPixel(10, getContext());
        mOutBound = new RectFloat();


        setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                int padding = AttrHelper.vp2px(2, getContext());
                mOutBound.modify(padding, padding, getWidth() - padding,getHeight() - padding);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });

    }



    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawRoundRect(mOutBound, mRadius, mRadius, mOuterPaint);
        canvas.drawRoundRect(mInBound, mRadius, mRadius, mInnerPaint);
    }


    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {

        int widthDimension = AttrHelper.vp2px(100, getContext());
        int heightDimension = AttrHelper.vp2px(20, getContext());

        setEstimatedSize(
                Component.EstimateSpec.getSizeWithMode(widthDimension, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getSizeWithMode( heightDimension, Component.EstimateSpec.NOT_EXCEED));

        return true;

    }

    @Override
    public void setMax(int max) {
        this.mMax = max;
    }

    @Override
    public void setProgress(int progress) {
        this.mProgress = progress;
        mInBound.modify(mBoundGap, mBoundGap,
                (getWidth() - mBoundGap) * mProgress / mMax, getHeight() - mBoundGap);
        invalidate();
    }


}

/*
 *    Copyright 2015 Kaopiz Software Co., Ltd.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.kaopiz.progresshud.kprogresshud;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.io.InputStream;

public class SpinView extends Component implements Indeterminate,Component.DrawTask,Component.EstimateSizeListener{
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(HiLog.LOG_APP, 0, "SpinView");
    private float mRotateDegrees;
    private int mFrameTime;
    private boolean mNeedToUpdateView;
    private Runnable mUpdateViewRunnable;

    public SpinView(Context context) {
        super(context);
        init();
    }

    public SpinView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
        addDrawTask(this);
        //setPixelMap(ResourceTable.Media_kprogresshud_spinner);
        mFrameTime = 1000 / 12;
        mUpdateViewRunnable = new Runnable() {
            @Override
            public void run() {
                mRotateDegrees += 30;
                mRotateDegrees = mRotateDegrees < 360 ? mRotateDegrees : mRotateDegrees - 360;
                invalidate();
                if (mNeedToUpdateView) {
                    getContext().getUITaskDispatcher().delayDispatch(this, mFrameTime);
                }
            }
        };

        setBindStateChangedListener(new BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                mNeedToUpdateView = true;
                getContext().getUITaskDispatcher().asyncDispatch(mUpdateViewRunnable);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                mNeedToUpdateView = false;

            }
        });
    }

    @Override
    public void setAnimationSpeed(float scale) {
        mFrameTime = (int) (1000 / 12 / scale);
    }



    @Override
    public void onDraw(Component component, Canvas canvas) {

        PixelMap pixelMap = getSpinPixelMap();
        int imageHeight = pixelMap.getImageInfo().size.height;
        int imageWidth = pixelMap.getImageInfo().size.width;
        canvas.rotate(mRotateDegrees, imageWidth /(float)2, imageHeight /(float)2);

        PixelMapHolder pmh = new PixelMapHolder(pixelMap);
        canvas.drawPixelMapHolder(
                pmh,
                0,
                0,
                new Paint());

    }
    private PixelMap getSpinPixelMap() {
        InputStream drawableInputStream = null;
        PixelMap pixelMap = null;
        try {
            drawableInputStream = this.getResourceManager().getResource(ResourceTable.Media_kprogresshud_spinner);
            ImageSource imageSource = ImageSource.create(drawableInputStream, new ImageSource.SourceOptions());
            ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
            pixelMap = imageSource.createPixelmap(options);
            return pixelMap;
        } catch (IOException | NotExistException exception) {
            HiLog.error(LABEL_LOG, "IOException | NotExistException", exception.getMessage());
        } finally {
            if (drawableInputStream != null) {
                try {
                    drawableInputStream.close();
                } catch (IOException e) {
                    HiLog.error(LABEL_LOG, "IOException", e.getMessage());
                }
            }
        }
        return pixelMap;
    }

    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        PixelMap pixelMap = getSpinPixelMap();
        int imageHeight = pixelMap.getImageInfo().size.height;
        int imageWidth = pixelMap.getImageInfo().size.width;
        setEstimatedSize(
                Component.EstimateSpec.getSizeWithMode(imageWidth, Component.EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getSizeWithMode(imageHeight, Component.EstimateSpec.NOT_EXCEED));
        return true;
    }
}

# ohos-loading-dialog
![image](./images/kprogresshud.gif)

#### 项目介绍
- 功能： 是ProgressHUD 的一个实现，类似于MBProgressHUD、SVProgressHUD的进度条样式。


#### 安装教程


方式一

1. 在需要使用的模块的build.gradle中引入

```
dependencies {
    compile project(path: ':kprogresshud')
}
```

在sdk5，DevEco Studio2.2 Bate1下项目可直接运行

方式二

在project的build.gradle中添加mavenCentral()的引用

```
repositories {   
 	...   
 	mavenCentral()   
	 ...           
 }
```

在entry的build.gradle中添加依赖

```
dependencies { 
... 
implementation 'com.gitee.archermind-ti:kprogresshud:1.0.0' 

... 
}
```
#### 使用说明
```
KProgressHUD 的使用非常简单。 您创建 HUD，自定义其样式并将其显示在 UI 线程上。 然后启动一个后台线程来处理长时间运行的任务完成后，调用dismiss() 关闭它（如果您使用确定的样式，如果进度达到最大值，HUD 将自动关闭）。

不确定的HUD
KProgressHUD.create(this)
	.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
	.setLabel("Please wait")
	.setDetailsLabel("Downloading data");
	.setCancellable(true)
	.setAnimationSpeed(2)
	.setDimAmount(0.5f)
	.show();

确定的HUD
KProgressHUD hud = KProgressHUD.create(this)
					.setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
					.setLabel("Please wait")
					.setMaxProgress(100)
					.show();
hud.setProgress(90);

或者，您可以创建自定义视图并传递给 HUD 以显示它。

 Image image = new Image(this);
 image.setPixelMap(ResourceTable.Media_ic_launcher);
KProgressHUD.create(this)
	.setCustomView(image)
    .setLabel("This is a custom view")
	.show();
自定义视图可以实现确定或不确定，这使得 HUD 将此视图视为默认的确定或不确定实现，而不是必需的。
```

#### 版本迭代

- v1.0.0

#### 基线release版本
master
commit id  0c49f8294ba686bd17446db7266cb9e1d6264b5d

#### 版权和许可信息

   Copyright 2015 Kaopiz Software Co., Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
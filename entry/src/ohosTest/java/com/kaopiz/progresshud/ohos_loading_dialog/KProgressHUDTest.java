package com.kaopiz.progresshud.ohos_loading_dialog;

import com.kaopiz.progresshud.kprogresshud.KProgressHUD;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.*;

public class KProgressHUDTest {
    @Test
    public void testKProgressHUD() {
        KProgressHUD   hud = KProgressHUD.create(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        assertNotNull(hud);
    }
}

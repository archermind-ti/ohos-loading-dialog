package com.kaopiz.progresshud.ohos_loading_dialog.slice;


import com.kaopiz.progresshud.ohos_loading_dialog.ResourceTable;
import com.kaopiz.progresshud.kprogresshud.KProgressHUD;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import ohos.app.dispatcher.TaskDispatcher;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener  {
    private KProgressHUD hud;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button indeterminate = (Button) findComponentById(ResourceTable.Id_indeterminate);
        indeterminate.setClickedListener(this);

        Button labelIndeterminate = (Button) findComponentById(ResourceTable.Id_label_indeterminate);
        labelIndeterminate.setClickedListener(this);

        Button detailIndeterminate = (Button) findComponentById(ResourceTable.Id_detail_indeterminate);
        detailIndeterminate.setClickedListener(this);

        Button determinate = (Button) findComponentById(ResourceTable.Id_determinate);
        determinate.setClickedListener(this);

        Button annularDeterminate = (Button) findComponentById(ResourceTable.Id_annular_determinate);
        annularDeterminate.setClickedListener(this);

        Button barDeterminate = (Button) findComponentById(ResourceTable.Id_bar_determinate);
        barDeterminate.setClickedListener(this);

        Button customView = (Button) findComponentById(ResourceTable.Id_custom_view);
        customView.setClickedListener(this);

        Button dimBackground = (Button) findComponentById(ResourceTable.Id_dim_background);
        dimBackground.setClickedListener(this);

        Button customColor = (Button) findComponentById(ResourceTable.Id_custom_color_animate);
        customColor.setClickedListener(this);

    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_indeterminate:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
                scheduleDismiss();
                break;
            case ResourceTable.Id_label_indeterminate:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("Please wait")
                        .setCancellable(true);
                scheduleDismiss();
                break;
            case ResourceTable.Id_detail_indeterminate:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setLabel("Please wait")
                        .setDetailsLabel("Downloading data");
                scheduleDismiss();
                break;
            case ResourceTable.Id_determinate:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.PIE_DETERMINATE)
                        .setLabel("Please wait");
                simulateProgressUpdate();
                break;
            case ResourceTable.Id_annular_determinate:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
                        .setLabel("Please wait");
                simulateProgressUpdate();
                break;
            case ResourceTable.Id_bar_determinate:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.BAR_DETERMINATE)
                        .setLabel("Please wait");
                simulateProgressUpdate();
                break;
            case ResourceTable.Id_custom_view:
                Image imageView = new Image(this);
                imageView.setPixelMap(ResourceTable.Media_ic_launcher);
                hud = KProgressHUD.create(this)
                        .setCustomView(imageView)
                        .setLabel("This is a custom view");
                scheduleDismiss();
                break;
            case ResourceTable.Id_dim_background:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setDimAmount(0.5f);
                scheduleDismiss();
                break;
            case ResourceTable.Id_custom_color_animate:
                hud = KProgressHUD.create(this)
                        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setWindowColor(getColor(ResourceTable.Color_colorPrimary))
                        .setAnimationSpeed(2);
                scheduleDismiss();
                break;
        }

        hud.show();
    }

    private void simulateProgressUpdate() {
        hud.setMaxProgress(100);
        TaskDispatcher globalTaskDispatcher = getUITaskDispatcher();

        globalTaskDispatcher.delayDispatch(new Runnable() {
            private int currentProgress;
            @Override
            public void run() {
                currentProgress += 1;
                hud.setProgress(currentProgress);
                if (currentProgress < 100) {
                    globalTaskDispatcher.delayDispatch(this, 50);
                }
            }
        }, 100);

    }

    private void scheduleDismiss() {
        TaskDispatcher globalTaskDispatcher = getUITaskDispatcher();
        globalTaskDispatcher.delayDispatch(new Runnable() {
            @Override
            public void run() {
                hud.dismiss();

            }
        }, 3000);
    }
}
